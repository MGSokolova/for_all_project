var mytrack = document.getElementById('mytrack');
var playButton = document.getElementById('playButton');

var duration = document.getElementById('fullDuration');
var currentTime = document.getElementById('currentTime');

var barSize = 840;
var bar = document.getElementById('defaultBar');
var progressBar = document.getElementById('progressBar');


playButton.addEventListener('click', playOrPause, false);
bar.addEventListener('click', clikedBar, false);


setTimeout(()=>{
    var minutes = parseInt(mytrack.duration/60);
   var seconds = parseInt(mytrack.duration%60);
   duration.innerHTML = minutes + ':' + seconds;
},500);


function playOrPause(){
    if(!mytrack.paused && !mytrack.ended){
        mytrack.pause();
        playButton.style.backgroundImage = 'url(./img/circled-play.png)';
        window.clearInterval(updateTime);
    }else{
        mytrack.play();
        playButton.style.backgroundImage = 'url(./img/pausee.png)';
        updateTime= setInterval(update, 500);
    }
}

function update(){
    if(!mytrack.ended){
        var playedMinutes = parseInt(mytrack.currentTime/60);
        var playedSeconds =  pad(parseInt(mytrack.currentTime%60));
        currentTime.innerHTML = playedMinutes + ':' + playedSeconds;
        
        var size = parseInt(mytrack.currentTime*barSize/mytrack.duration);
        progressBar.style.width = size + "px";

    }else{
        currentTime.innerHTML = "0.00";
        playButton.style.backgroundImage = 'url(./img/play-icon.png)';
        progressBar.style.width = "0px";
        window.clearInterval(updateTime);
    }
}

function clikedBar(e){
    if(!mytrack.ended){
        var mouseX = e.pageX - bar.offsetLeft;
        var newTime = mouseX * mytrack.duration / barSize;
        mytrack.currentTime = newTime;
        progressBar.style.width = mouseX + "px";
    }
}

function pad(d){
    return (d <10) ? "0" + d.toString() : d.toString();
}


//chart
const ctx = document.getElementById("lineChart");

let lineCh =  new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["22.04", "23.04","24.04","25.04","26.04","27.04","28.04","29.04","30.04"],
        datasets: [{
             data: [18, 23.8, 5, 13, 16,5, 14, 16,6, 16.8, 13,2],
            fill: false,
            lineTension: 0,
            borderColor: 'orange',
            backgroundColor: 'transparent',
            pointBorderColor: 'orange',
            pointBackgroundColor: 'rgba(255,150,0,0.5)',
            pointRadius: 1,
            pointHoverRadius: 6,
            pointHitRadius: 30,
            pointBorderWidth: 2,
            pointStyle: 'rectRounded' 
        }]
    },
    options: {
        scales: {
            yAxes:[{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
})

function openPage(evt, eventName){
    var  i, tabcontent, tablinks;

    tabcontent= document.getElementsByClassName("tabcontent");
    for ( i=0; i<tabcontent.length; i++){
        tabcontent[i].style.display = "none";
    }

    tablinks= document.getElementsByClassName("tablinks");
    for ( i=0; i<tablinks.length; i++){
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    document.getElementById(eventName).style.display="block";
    evt.currentTarget.className += " active";
 
}

document.getElementById("defaultOpen").click();

 

